COIs := $(wildcard data/raw/coi/*.fa)

#
# these four rules concatenate all COI files, modify the headers, and make the alignment.
#
#
data/clean/coi/COIs.fa: $(COIs)
	cat $(COIs) data/clean/sanger/NE489_La_2_COI.LCO-1490.fa \
	| seqkit grep --by-name --invert-match --use-regexp --pattern 'Nemertea' \
	| seqkit grep --by-name --invert-match --use-regexp --pattern 'UNVERIFIED' \
	| seqkit replace -p '(La_2_COI.LCO-1490)' -r '$$1 Lineus acutifrons cytochrome c oxidase subunit I (COI) gene, partial cds; mitochondrial' \
	| seqkit replace -p '(CA4_coI.LCO-1490)' -r '$$1 Carinoma armandi cytochrome c oxidase subunit I (COI) gene, partial cds; mitochondrial' \
	| seqkit replace -p '(Lineus)_(acutifrons)_(GU590937)' -r '$$3 $$1 $$2 cytochrome c oxidase subunit I (COI) gene, partial cds; mitochondrial' \
	| seqkit replace -p '(Lacu[23]Hcorrected)' -r '$$1 Lineus acutifrons II cytochrome c oxidase subunit I (COI) gene, partial cds; mitochondrial' \
	| seqkit replace -p '(LacuHcorrected)' -r '$$1 Lineus acutifrons I cytochrome c oxidase subunit I (COI) gene, partial cds; mitochondrial' \
	| seqkit replace -p '(LacuBonn)' -r '$$1 Lineus acutifrons cytochrome c oxidase subunit I (COI) gene, partial cds; mitochondrial' \
	| seqkit replace -p 'Nipponnemertes bimaculatus' -r 'Nipponnemertes bimaculata' \
	| seqkit replace -p 'Cephalothrix oestrymnica' -r 'Cephalothrix oestrymnicus' \
	| seqkit replace -p 'Ramphogordius lacteus' -r 'Lineus lacteus' \
	| seqkit rmdup --by-name \
	> $@
results/coi-tree/COIs.fa.mod: data/clean/COIs.fa
	sed -e '/^>/s/ /_/' -e '/^>/s/ /_/' $^ > $@
results/coi-tree/COIs.fa.mod.rmdup: results/coi-tree/COIs.fa.mod
	seqkit rmdup --by-name $^ > $@
results/coi-tree/COIs.fa.mod.rmdup.aln: results/coi-tree/COIs.fa.mod.rmdup
	mafft --thread 6 $^ > $@

#
# This rule makes the tree.
#
results/coi-tree/COIs.fa.mod.rmdup.aln.treefile: results/coi-tree/COIs.fa.mod.rmdup.aln
	iqtree -nt 6 -s $^

#
# these two rules select the longest COI sequence for each species.
#
results/cois/01_longest-COIs.tsv: data/clean/coi/COIs.fa code/select-longest-COIs.R
	seqkit fx2tab -nl $< > scratch/COIs.fa.lens.tsv
	Rscript code/select-longest-COIs.R scratch/COIs.fa.lens.tsv $@ # this script creates the list of longest COIs
results/cois/02_longest-COIs.fa: results/cois/01_longest-COIs.tsv data/clean/coi/COIs.fa
	tail -n +2 $^ | cut -f 1 > scratch/ids.txt
	seqkit grep -rn -f scratch/ids.txt data/clean/coi/COIs.fa \
	> $@
results/cois/03_COIs-for-branch-length-estimation.fa: results/cois/02_longest-COIs.fa
	nw_labels data/clean/Nemertea.cladogram.tre > scratch/species-in-tree.txt
	python3 code/rename-NCBI-COIs.py $^ \
	| seqkit replace -p 'Ramphogordius' -r 'Lineus' \
	| seqkit rmdup -n \
	| seqkit grep -f scratch/species-in-tree.txt > $@
results/cois/04_COIs-for-branch-length-estimation.mafft.afa: results/cois/03_COIs-for-branch-length-estimation.fa
	mafft $^ > $@
results/2019-06-06_tree-with-branch-lengths/04_COIs-for-branch-length-estimation.mafft.afa.treefile: results/cois/04_COIs-for-branch-length-estimation.mafft.afa
	iqtree -nt 6 -s $^


treefile := results/2019-06-06_tree-with-branch-lengths/04_COIs-for-branch-length-estimation.mafft.afa.treefile 
coilist := results/cois/01_longest-COIs.tsv
results/2019-07-02_final-list-of-ncbi-sequences/S1_Accession_numbers.txt: $(treefile) $(coilist)
	nw_labels $(treefile) > scratch/in-tree
	(head -n 1 $(coilist); grep -f scratch/in-tree $(coilist)) > $@
