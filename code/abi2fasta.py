#!/usr/bin/python3

from Bio import SeqIO
import sys

infile = sys.argv[1]

for record in SeqIO.parse(infile, "abi"):
    print(">%s\n%s" % (record.id, record.seq))
