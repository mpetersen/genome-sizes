#!/usr/bin/env python3

from ete3 import Tree, NodeStyle, TreeStyle

tree = Tree("doc/Nemertea.tre")

ns = NodeStyle()
ns['size'] = 0
ns['hz_line_width'] = 2
ns['vt_line_width'] = 2

nst1 = NodeStyle(ns)
nst1['bgcolor'] = 'PaleTurquoise'
nst2 = NodeStyle(ns)
nst2['bgcolor'] = 'Bisque'
nst3 = NodeStyle(ns)
nst3['bgcolor'] = 'PaleGreen'

for node in tree.traverse():
    node.dist = 1
    node.support = 1
    node.set_style(ns)

n1 = tree.get_common_ancestor("Tubulanus_polymorphus", "Carinina_ochracea")
n1.set_style(nst1)
n2 = tree.get_common_ancestor("Micrura_verrilli", "Cerebratulus_marginatus")
n2.set_style(nst2)
n3 = tree.get_common_ancestor("Emplectonema_gracile", "Nipponnemertes_bimaculata")
n3.set_style(nst3)

ts = TreeStyle()
ts.branch_vertical_margin = 10
ts.show_scale = False

tree.write(outfile = "doc/Nemertea.cladogram.tre")
tree.render("doc/Nemertea.cladogram.pdf", tree_style = ts)
